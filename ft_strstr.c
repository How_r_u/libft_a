/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:56:05 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 18:54:11 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *strb, const char *stra)
{
	size_t	i;
	size_t	j;
	size_t	k;
	char	*ret;

	i = -1;
	j = 0;
	k = -1;
	ret = (char *)strb;
	if (stra[j] == '\0')
		return (ret);
	while (strb[++i])
		if (strb[i] == stra[j])
			while (stra[++j] == '\0')
				return (&ret[i - j + 1]);
		else
		{
			j = 0;
			i = ++k;
		}
	return (NULL);
}
