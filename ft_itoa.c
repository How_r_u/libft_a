/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 19:36:03 by lshanaha          #+#    #+#             */
/*   Updated: 2018/12/01 21:19:58 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static char	*ft_helper(char *new_str)
{
	new_str[0] = '0';
	new_str[1] = '\0';
	return (new_str);
}

static void	ft_strrevers(char *s)
{
	int		i;
	int		len;
	char	first;

	if (s == NULL)
		return ;
	len = ft_strlen(s) - 1;
	i = 0;
	while (i < len - i)
	{
		first = s[i];
		s[i] = s[len - i];
		s[len - i] = first;
		i++;
	}
}

char		*ft_itoa(int n)
{
	char	*new_str;
	int		i;
	int		j;

	i = 0;
	j = 1;
	if (!(new_str = (char *)malloc(ft_intlen(n) + 1)))
		return (NULL);
	if (n == 0)
		return (ft_helper(new_str));
	if (n < 0)
	{
		new_str[i++] = -(n % 10) + '0';
		n = n / 10;
		j = -1;
		n = j * n;
	}
	(n != 0) ? (new_str[i++] = n % 10 + '0') : (n);
	while ((n = (n / 10)))
		(new_str[i++]) = n % 10 + '0';
	if (j == -1)
		(new_str[i++]) = '-';
	(new_str[i]) = '\0';
	ft_strrevers(new_str);
	return (new_str);
}
