/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 20:13:35 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 18:42:32 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int ch)
{
	int		i;
	char	*ret;

	i = ft_strlen(str);
	ret = (char *)str;
	while (i >= 0)
	{
		if (str[i] == ch)
			return (&ret[i]);
		i--;
	}
	return (NULL);
}
