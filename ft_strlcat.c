/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 17:52:07 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 17:28:05 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	if (n == 0)
		return (ft_strlen(src));
	while (dst[i])
		i++;
	while (src[j] && (i + j) < (n - 1))
	{
		dst[i + j] = src[j];
		j++;
	}
	dst[i + j] = '\0';
	while ((i + j) < (n - 1) || dst[i + j])
	{
		dst[i + j] = '\0';
		j++;
	}
	return ((size_t)((ft_strlen(dst) < n) ? (ft_strlen(src) + i) :\
	(ft_strlen(src) + n)));
}
