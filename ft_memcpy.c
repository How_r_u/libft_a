/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:19:40 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 21:15:01 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char	*dest_arr;
	unsigned char	*src_arr;

	dest_arr = (unsigned char *)dest;
	src_arr = (unsigned char *)src;
	if (dest == src)
		return (dest);
	while (n--)
		*dest_arr++ = *src_arr++;
	return (dest);
}
