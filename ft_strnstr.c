/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:55:10 by lshanaha          #+#    #+#             */
/*   Updated: 2018/12/01 21:15:09 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *hay, const char *needle, size_t len)
{
	size_t	i;
	size_t	j;
	size_t	k;

	i = -1;
	j = 0;
	k = 0;
	if (needle[j] == '\0')
		return ((char *)hay);
	if (((ft_strlen(needle) > len && len != 0) || (ft_strlen(needle) >\
	ft_strlen(hay))) && hay != NULL)
		return (0x0);
	while (hay[++i] && i < len)
	{
		if (hay[i] == needle[j])
			while (needle[++j] == '\0' || j == len)
				return ((char *)(hay + i - j + 1));
		else
		{
			j = 0;
			i = ++k;
		}
	}
	return (NULL);
}
