/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:10:57 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 18:23:01 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *destination, const char *append, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (destination[i])
		i++;
	while (append[j] && j < n)
	{
		destination[i + j] = append[j];
		j++;
	}
	if (j <= n)
		destination[i + j] = '\0';
	return (destination);
}
