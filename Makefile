# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/20 21:12:50 by how_r_u           #+#    #+#              #
#    Updated: 2018/12/01 21:17:09 by lshanaha         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS =	ft_strncpy.c\
 ft_strcmp.c\
 ft_intlen.c\
 ft_lstmap.c\
 ft_lstdelone.c\
 ft_putendl_fd.c\
 ft_striter.c\
 ft_lstdel.c\
 ft_toupper.c\
 ft_strcpy.c\
 ft_isalpha.c\
 ft_striteri.c\
 ft_bzero.c\
 ft_strchr.c\
 ft_putchar_fd.c\
 ft_lstclear.c\
 ft_strrev.c\
 ft_putendl.c\
 ft_strnew.c\
 ft_strsub.c\
 ft_isprint.c\
 ft_memdel.c\
 ft_itoa.c\
 ft_strnstr.c\
 ft_lstiter.c\
 ft_tolower.c\
 ft_strcat.c\
 ft_atoi.c\
 ft_lstnew.c\
 ft_char_in_str.c\
 ft_strjoin.c\
 ft_strnequ.c\
 ft_strmapi.c\
 ft_isdigit.c\
 ft_strdel.c\
 ft_isalnum.c\
 ft_memccpy.c\
 ft_putnbr.c\
 ft_memmove.c\
 ft_strsplit.c\
 ft_lstadd.c\
 ft_putstr_fd.c\
 ft_memchr.c\
 ft_strlen.c\
 ft_memalloc.c\
 ft_strrchr.c\
 ft_putstr.c\
 ft_strmap.c\
 ft_strlcat.c\
 ft_putnbr_fd.c\
 ft_strstr.c\
 ft_putchar.c\
 ft_memcpy.c\
 ft_isascii.c\
 ft_strdup.c\
 ft_strtrim.c\
 ft_memcmp.c\
 ft_strncat.c\
 ft_strclr.c\
 ft_memset.c\
 ft_strncmp.c\
 ft_strequ.c\
 ft_btree_apply_infix.c\
 ft_btree_apply_prefix.c\
 ft_btree_apply_suffix.c\
 ft_btree_create_node.c\
 ft_btree_insert.c\
 ft_btree_search.c\
 ft_lstadd_last.c

NAME = libft.a
OBJ = $(SRCS:%.c=%.o)
FLAGS = -Wall -Werror -Wextra -c -I ./

all: $(NAME)

$(NAME): $(OBJ)
	ar rcs $(NAME) $(OBJ)
	ranlib $(NAME)

$(OBJ): $(SRCS)
	gcc $(FLAGS) $(SRCS)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
