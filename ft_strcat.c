/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 20:08:38 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 16:51:14 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *destination, const char *append)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (destination[i])
		i++;
	while (append[j])
	{
		destination[i + j] = append[j];
		j++;
	}
	destination[i + j] = '\0';
	return (destination);
}
