/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 15:45:30 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 14:47:38 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	char	*arr;
	size_t	i;

	if (n == 0)
		return ;
	arr = (char *)s;
	i = 0;
	while (i < n)
		arr[i++] = '\0';
	return ;
}
