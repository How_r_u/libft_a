/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lshanaha <lshanaha@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 15:18:11 by lshanaha          #+#    #+#             */
/*   Updated: 2018/11/29 16:36:36 by lshanaha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

void	ft_putstr_fd(char const *s, int fd)
{
	size_t		i;

	if (fd < 0)
		return ;
	i = 0;
	if (s == 0x0)
		return ;
	while (s[i])
	{
		write(fd, &s[i], 1);
		i++;
	}
}
